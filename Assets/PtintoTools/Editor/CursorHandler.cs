﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

//[InitializeOnLoad] makes sure that the constructor of this class is called in the editor
//whenever the editor assembly is being loaded (for example on start or on recompile)
[InitializeOnLoad]
public class CursorHandler : Editor
{
    public class PainterQuad
    {
        public Vector2 position;
        public Sprite tileSprite;
    }

    public static MSRoom currentRoom;
    public static Vector3 CurrentHandlePosition = Vector3.zero;
    public static bool IsMouseInValidArea = false;

    static Vector3 m_OldHandlePosition = Vector3.zero;
    static Vector3 lastPaintedPos;
    static bool mouseHeld = false;
    static Vector2 rectHoldStart, rectHoldEnd;
    static float tileSizeMultiplier = 1f;
    static GameObject previewObj;

    static CursorHandler()
    {
        //The OnSceneGUI delegate is called every time the SceneView is redrawn and allows you
        //to draw GUI elements into the SceneView to create in editor functionality
        SceneView.onSceneGUIDelegate -= OnSceneGUI;
        SceneView.onSceneGUIDelegate += OnSceneGUI;
    }
    

    void OnDestroy()
    {
        SceneView.onSceneGUIDelegate -= OnSceneGUI;
    }
    
    static void OnSceneGUI(SceneView sceneView)
    {
        if(CheckForKeepExecution() == false)
        {
            if (previewObj != null)
                DestroyImmediate(previewObj);
            Tools.hidden = false;
            return;
        }

        //Hide Tools and prepare

        Tools.hidden = true;
        HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        if (currentRoom.spr != null)
            tileSizeMultiplier = (currentRoom.spr as Sprite).rect.width / (currentRoom.spr as Sprite).pixelsPerUnit;

        //Update things

        UpdateHandlePosition();
        UpdateIsMouseInValidArea(sceneView.position);
        UpdateRepaint();

        //Draw screen, first the handlers, then the Scene View GUI
        if(currentRoom.levelAssets != null)
        {
            DrawHandlesAndMenus();

            CaptureEvents();

            SceneView.RepaintAll();
        }

    }
    static void DrawHandlesAndMenus()
    {
        switch (EditorPrefs.GetInt("MSOptionSelected", 1))
        {
            case 1:
                DrawSquareDrawPreview();
                DrawTileScreenMenu();
                break;
            case 2:
                if (previewObj != null)
                    DestroyImmediate(previewObj);
                DrawColliderPreview();
                DrawColliderScreenMenu();
                break;
            case 3:
                if (previewObj != null)
                    DestroyImmediate(previewObj);
                break;
            default:
                if (previewObj != null)
                    DestroyImmediate(previewObj);
                break;

        }

    }
    static void DrawColliderScreenMenu()
    {
        Handles.BeginGUI();
        Rect guiContentRect = new Rect(10, 20, 40, 121);

        EditorGUI.DrawRect(guiContentRect, new Color(0, 0.3f, 0.6f, 0.5f));
        if (GUI.Button(new Rect(14, 25, 32, 32), Resources.Load("icon-01") as Texture2D) || Event.current.control)
        {
            //Debug.Log("Cursor y seleccionar");
            EditorPrefs.SetInt("CollidersHandleEnabled", 0);
            currentRoom.GenerateTileLayer();
        }
        if (GUI.Button(new Rect(14, 62, 32, 32), Resources.Load("icon-04") as Texture2D))
        {
            //Debug.Log("Dibujar Collider");
            EditorPrefs.SetInt("CollidersHandleEnabled", 1);
            currentRoom.GenerateTileLayer();
        }
        if (GUI.Button(new Rect(14, 99, 32, 32), Resources.Load("icon-03") as Texture2D))
        {
            //Debug.Log("Eliminar Collider");
            EditorPrefs.SetInt("CollidersHandleEnabled", 2);
            currentRoom.GenerateTileLayer();
        }

        Handles.EndGUI();

    }
    static void DrawTileScreenMenu()
    {
        Handles.BeginGUI();
        Rect guiContentRect = new Rect(10, 20, 40, 158);

        EditorGUI.DrawRect(guiContentRect, new Color(0, 0.3f, 0.6f, 0.5f));
        if (GUI.Button(new Rect(14, 25, 32, 32), Resources.Load("icon-01") as Texture2D) || Event.current.control)
        {
            //Debug.Log("Cursor");
            EditorPrefs.SetInt("BasicHandleEnabled", 0);
            currentRoom.GenerateTileLayer();
        }
        if (GUI.Button(new Rect(14, 62, 32, 32), Resources.Load("icon-02") as Texture2D))
        {
            //Debug.Log("Pincel");
            EditorPrefs.SetInt("BasicHandleEnabled", 1);
            currentRoom.GenerateTileLayer();

        }
        if (GUI.Button(new Rect(14, 99, 32, 32), Resources.Load("icon-03") as Texture2D))
        {
            //Debug.Log("Borrador");
            EditorPrefs.SetInt("BasicHandleEnabled", 2);
            currentRoom.GenerateTileLayer();
        }
        if (GUI.Button(new Rect(14, 136, 32, 32), Resources.Load("icon-04") as Texture2D))
        {
            //Debug.Log("Rectangulo");
            EditorPrefs.SetInt("BasicHandleEnabled", 3);
            currentRoom.GenerateTileLayer();
        }

        Handles.EndGUI();

    }

    /// <summary>
    /// Mouse Events Capture
    /// </summary>
    
    static void CaptureEvents()
    {
        //Debug.Log("MSOptions = " + EditorPrefs.GetInt("MSOptionSelected", 0));
        switch(EditorPrefs.GetInt("MSOptionSelected", 1))
        {
            case 1:
                //Debug.Log("BasicHandleEnabled = " + EditorPrefs.GetInt("BasicHandleEnabled", 0));
                switch (EditorPrefs.GetInt("BasicHandleEnabled", 0))
                {
                    case 1:
                        CaptureBrushEvents();
                        break;
                    case 2:
                        CaptureEraseEvents();
                        break;
                    case 3:
                        CaptureRectangleEvents();
                        break;
                    default:
                        break;
                }
                break;
            case 2:
                //Debug.Log("CollidersHandleEnabled = " + EditorPrefs.GetInt("CollidersHandleEnabled", 0));
                switch (EditorPrefs.GetInt("CollidersHandleEnabled", 0))
                {
                    case 0:
                        //Debug.Log("CAPTURAR EVENTOS DE SELECCIONAR");
                        break;
                    case 1:
                        CaptureCollidersEvents();
                        break;
                    case 2:
                        break;
                    default:
                        break;
                }
                break;
            case 3:
                break;
            default:
                break;

        }
    }

    static void CaptureCollidersEvents()
    {
        Vector2 mousePos = new Vector2(Event.current.mousePosition.x, Event.current.mousePosition.y);
        if (!mouseHeld)
            rectHoldStart = GetTilePosition(HandleUtility.GUIPointToWorldRay(mousePos).origin);

        rectHoldEnd = GetTilePosition(HandleUtility.GUIPointToWorldRay(mousePos).origin);

        if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
        {
            mouseHeld = true;
        }
        else if (Event.current.type == EventType.MouseUp && Event.current.button == 0)
        {
            mouseHeld = false;
            Vector2 tileStart, tileEnd;
            tileStart = new Vector2(Mathf.Min(rectHoldStart.x, rectHoldEnd.x), Mathf.Max(rectHoldStart.y, rectHoldEnd.y));
            tileEnd = new Vector2(Mathf.Max(rectHoldStart.x, rectHoldEnd.x), Mathf.Min(rectHoldStart.y, rectHoldEnd.y));
            PaintColliderRectangle(tileStart, new Vector2((tileEnd.x - tileStart.x + tileSizeMultiplier), (tileStart.y - tileEnd.y + tileSizeMultiplier)), Event.current.shift);
        }
        if (mouseHeld)
        {
            Vector2 tileStart, tileEnd;
            tileStart = new Vector2(Mathf.Min(rectHoldStart.x, rectHoldEnd.x), Mathf.Max(rectHoldStart.y, rectHoldEnd.y));
            tileEnd = new Vector2(Mathf.Max(rectHoldStart.x, rectHoldEnd.x), Mathf.Min(rectHoldStart.y, rectHoldEnd.y));

        }
    }

    static void CaptureEraseEvents()
    {
        Vector2 mousePos = new Vector2(Event.current.mousePosition.x, Event.current.mousePosition.y);
        if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
        {
            mouseHeld = true;
            //Debug.Log(HandleUtility.PickRectObjects(new Rect(mousePos, new Vector2(100, 100)), false).Length);
            if (currentRoom != null)
            {
                lastPaintedPos = GetTilePosition(HandleUtility.GUIPointToWorldRay(mousePos).origin);
                currentRoom.EraseTile(lastPaintedPos);
            }
        }
        else if (Event.current.type == EventType.MouseUp && Event.current.button == 0)
        {
            mouseHeld = false;
            //Debug.Log("release");
        }
        if (mouseHeld)
        {
            if (currentRoom != null)
            {
                if (lastPaintedPos != GetTilePosition(HandleUtility.GUIPointToWorldRay(mousePos).origin))
                {
                    //Debug.Log("cambio de grid, pintate algo");
                    lastPaintedPos = GetTilePosition(HandleUtility.GUIPointToWorldRay(mousePos).origin);
                    currentRoom.EraseTile(lastPaintedPos);
                }
            }
        }

    }

    static void CaptureBrushEvents()
    {
        if(Event.current.shift)
        {
            CaptureEraseEvents();
            return;
        }
        Vector2 mousePos = new Vector2(Event.current.mousePosition.x, Event.current.mousePosition.y);
        if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
        {
            mouseHeld = true;
            //Debug.Log(HandleUtility.PickRectObjects(new Rect(mousePos, new Vector2(100, 100)), false).Length);
            if (currentRoom != null)
            {
                lastPaintedPos = GetTilePosition(HandleUtility.GUIPointToWorldRay(mousePos).origin);
                currentRoom.PaintTile(lastPaintedPos);
            }
        }
        else if (Event.current.type == EventType.MouseUp && Event.current.button == 0)
        {
            mouseHeld = false;
            //Debug.Log("release");
        }
        if (mouseHeld)
        {
            if (currentRoom != null)
            {
                if (lastPaintedPos != GetTilePosition(HandleUtility.GUIPointToWorldRay(mousePos).origin))
                {
                    //Debug.Log("cambio de grid, pintate algo");
                    lastPaintedPos = GetTilePosition(HandleUtility.GUIPointToWorldRay(mousePos).origin);
                    currentRoom.PaintTile(lastPaintedPos);
                }
            }
        }
    }

    static void CaptureRectangleEvents()
    {
        Vector2 mousePos = new Vector2(Event.current.mousePosition.x, Event.current.mousePosition.y);
        if (!mouseHeld)
            rectHoldStart = GetTilePosition(HandleUtility.GUIPointToWorldRay(mousePos).origin);

        rectHoldEnd = GetTilePosition(HandleUtility.GUIPointToWorldRay(mousePos).origin);

        if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
        {
            mouseHeld = true;
        }
        else if (Event.current.type == EventType.MouseUp && Event.current.button == 0)
        {
            mouseHeld = false;
            Vector2 tileStart, tileEnd;
            tileStart = new Vector2(Mathf.Min(rectHoldStart.x, rectHoldEnd.x), Mathf.Max(rectHoldStart.y, rectHoldEnd.y));
            tileEnd = new Vector2(Mathf.Max(rectHoldStart.x, rectHoldEnd.x), Mathf.Min(rectHoldStart.y, rectHoldEnd.y));
            PaintRectangle(tileStart, new Vector2((tileEnd.x - tileStart.x + tileSizeMultiplier), (tileStart.y - tileEnd.y + tileSizeMultiplier)), Event.current.shift);
        }
        if (mouseHeld)
        {
            Vector2 tileStart, tileEnd;
            tileStart = new Vector2(Mathf.Min(rectHoldStart.x, rectHoldEnd.x), Mathf.Max(rectHoldStart.y, rectHoldEnd.y));
            tileEnd = new Vector2(Mathf.Max(rectHoldStart.x, rectHoldEnd.x), Mathf.Min(rectHoldStart.y, rectHoldEnd.y));

        }
    }

    /// <summary>
    /// Drawing the preview
    /// </summary>

    static void DrawColliderPreview()
    {
        if (IsMouseInValidArea == false)
        {
            return;
        }
        Handles.color = Color.white;

        switch (EditorPrefs.GetInt("CollidersHandleEnabled", 0))
        {
            case 1:
                DrawHandlesRectangle(true);
                break;
            case 2:
                break;
            default:
                break;
        }

    }


    static void DrawSquareDrawPreview()
    {
        if (IsMouseInValidArea == false)
        {
            //Debug.Log("Si el objeto no es null, se destruye");
            if (previewObj != null)
                DestroyImmediate(previewObj);
            return;
        }
        //Debug.Log("Si el objeto es null, creo uno nuevo");

        Handles.color = Color.yellow;

        switch(EditorPrefs.GetInt("BasicHandleEnabled", 0))
        {
            case 1:
                DrawHandlesSquare(CurrentHandlePosition, false);
                break;
            case 2:
                DrawHandlesSquare(CurrentHandlePosition, true);
                if (previewObj != null)
                    DestroyImmediate(previewObj);
                break;
            case 3:
                DrawHandlesRectangle(false);
                break;
            default:
                if (previewObj != null)
                    DestroyImmediate(previewObj);
                break;
        }
    }

    static void DrawHandlesSquare(Vector2 center, bool erase)
    {
        Vector2 p1 = center + new Vector2(-0.5f, 0.5f) * tileSizeMultiplier;
        Vector2 p2 = center + new Vector2(0.5f, 0.5f) * tileSizeMultiplier;
        Vector2 p3 = center + new Vector2(0.5f, -0.5f) * tileSizeMultiplier;
        Vector2 p4 = center + new Vector2(-0.5f, -0.5f) * tileSizeMultiplier;

        //You can use Handles to draw 3d objects into the SceneView. If defined properly the
        //user can even interact with the handles. For example Unitys move tool is implemented using Handles
        //However here we simply draw a cube that the 3D position the mouse is pointing to
        Handles.DrawLine(p1, p2);
        Handles.DrawLine(p2, p3);
        Handles.DrawLine(p3, p4);
        Handles.DrawLine(p4, p1);
        
        //Debug.Log("Primero el mouse en " + CurrentHandlePosition);
        //Debug.Log("EL HANDLE LO PONGO EN " + p1);

        if (!erase && !Event.current.shift)
            DrawSprite(currentRoom.spr as Sprite, p1);

    }

    static void DrawHandlesRectangle(bool erase)
    {
        Vector2 tileStart, tileEnd;
        tileStart = new Vector2(Mathf.Min(rectHoldStart.x, rectHoldEnd.x), Mathf.Max(rectHoldStart.y, rectHoldEnd.y));
        tileEnd = new Vector2(Mathf.Max(rectHoldStart.x, rectHoldEnd.x), Mathf.Min(rectHoldStart.y, rectHoldEnd.y));

        Vector2 p1 = tileStart;
        Vector2 p2 = new Vector2(tileEnd.x + tileSizeMultiplier, tileStart.y);
        Vector2 p3 = new Vector2(tileEnd.x + tileSizeMultiplier, tileEnd.y - tileSizeMultiplier);
        Vector2 p4 = new Vector2(tileStart.x, tileEnd.y - tileSizeMultiplier);

        //You can use Handles to draw 3d objects into the SceneView. If defined properly the
        //user can even interact with the handles. For example Unitys move tool is implemented using Handles
        //However here we simply draw a cube that the 3D position the mouse is pointing to
        Handles.DrawLine(p1, p2);
        Handles.DrawLine(p2, p3);
        Handles.DrawLine(p3, p4);
        Handles.DrawLine(p4, p1);

        //if (!erase && !Event.current.shift)
        //    DrawSprite(currentRoom.spr as Sprite, p1);
        if (!erase && !Event.current.shift)
            DrawSpriteRect(currentRoom.spr as Sprite, p1, new Vector2((tileEnd.x - tileStart.x + tileSizeMultiplier), (tileStart.y - tileEnd.y + tileSizeMultiplier)));
    }

    static void PaintRectangle(Vector2 position, Vector2 sizeMultiplier, bool erase)
    {
        //Debug.Log(sizeMultiplier+" "+tileSizeMultiplier);
        Vector2 tilePos;
        for (float i = 0; i < sizeMultiplier.x; i += tileSizeMultiplier)
        {
            for (float j = 0; j < sizeMultiplier.y; j += tileSizeMultiplier)
            {
                if (currentRoom != null)
                {
                    tilePos = new Vector2(position.x + i, position.y - j);
                    if (!erase)
                        currentRoom.PaintTile(tilePos);
                    else
                        currentRoom.EraseTile(tilePos);
                }
            }
        }
    }

    static void PaintColliderRectangle(Vector2 position, Vector2 sizeMultiplier, bool erase)
    {
        //Debug.Log(sizeMultiplier+" "+tileSizeMultiplier);
        Vector2 tilePos;
        for (float i = 0; i < sizeMultiplier.x; i += tileSizeMultiplier)
        {
            for (float j = 0; j < sizeMultiplier.y; j += tileSizeMultiplier)
            {
                if (currentRoom != null)
                {
                    tilePos = new Vector2(position.x + i, position.y - j);
                    if (!erase)
                        currentRoom.PaintCollider(tilePos);
                    else
                        currentRoom.EraseCollider(tilePos);
                }
            }
        }
    }

    static void CreateSpriteObject()
    {
        if(previewObj == null)
        {
            previewObj = Instantiate(Resources.Load("PreviewObject")) as GameObject;
            //previewObj.hideFlags = HideFlags.HideAndDontSave;
        }
    }

    //For drawing a sprite preview
    static void DrawSprite(Sprite sprite, Vector3 position)
    {
        //Debug.Log("Ahora el handle en " + CurrentHandlePosition);
        //Debug.Log("PINTA SPRITE EN " + position);
        CreateSpriteObject();
        if (previewObj != null)
        {
            previewObj.GetComponent<SpriteRenderer>().sprite = sprite;
            previewObj.GetComponent<SpriteRenderer>().size = sprite.bounds.size;
            previewObj.transform.localPosition = position;
        }
    }

    //For drawing a sprite preview
    static void DrawSpriteRect(Sprite sprite, Vector2 position, Vector2 sizeMultiplier)
    {
        //Debug.Log(sizeMultiplier);
        if ((sizeMultiplier.x * sizeMultiplier.y) < 16384)
        {
            sizeMultiplier /= tileSizeMultiplier;
            CreateSpriteObject();
            if (previewObj != null)
            {
                previewObj.GetComponent<SpriteRenderer>().sprite = sprite;
                previewObj.GetComponent<SpriteRenderer>().size = new Vector2(sprite.bounds.size.x * sizeMultiplier.x, sprite.bounds.size.y * sizeMultiplier.y);
                previewObj.transform.localPosition = position;
            }
        }
    }

    static Vector3 GetTilePosition(Vector3 pos)
    {
        float posX = Mathf.Floor(pos.x / tileSizeMultiplier) * tileSizeMultiplier;
        float posY = Mathf.Ceil(pos.y / tileSizeMultiplier) * tileSizeMultiplier;
        
        return new Vector3(posX, posY, 0);
    }
    

    //I will use this type of function in many different classes. Basically this is useful to 
    //be able to draw different types of the editor only when you are in the correct scene so we
    //can have an easy to follow progression of the editor while hoping between the different scenes
    static bool IsInCorrectLevel()
    {
        return UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene().name == "Level1";
    }

    /// <summary>
    /// This method checks for an exit to the execution
    /// </summary>
    /// <returns></returns>

    static bool CheckForKeepExecution()
    {
        if (IsInCorrectLevel() == false)
        {
            return false;
        }
        if (Selection.gameObjects.Length == 0 || Selection.gameObjects.Length > 1)
        {
            return false;
        }

        //Check if the room in the selection is not null. If so, check if it is different from currentRoom.
        //If the room is different, regenerate the TileLayer

        MSRoom newAuxRoom = Selection.gameObjects[0].GetComponent<MSRoom>();

        //if (newAuxRoom != null)
        //{
        //    if (newAuxRoom != currentRoom)
        //        newAuxRoom.GenerateTileLayer();
        //}

        currentRoom = newAuxRoom;
        if (currentRoom == null)
        {
            return false;
        }
        return true;
    }


    static void UpdateIsMouseInValidArea(Rect sceneViewRect)
    {
        //Make sure the cube handle is only drawn when the mouse is within a position that we want
        //In this case we simply hide the cube cursor when the mouse is hovering over custom GUI elements in the lower
        //are of the sceneView which we will create in E07
        //bool isInValidArea = Event.current.mousePosition.y < sceneViewRect.height;

        //Only draw if it is inside the scene screen, so the mousePosition needs an offset from the up-left corner of the screen to the screen itself

        bool isInValidArea = sceneViewRect.Contains(Event.current.mousePosition + sceneViewRect.min, true);

        if (isInValidArea != IsMouseInValidArea)
        {
            IsMouseInValidArea = isInValidArea;
            SceneView.RepaintAll();
        }
    }

    static void UpdateHandlePosition()
    {
        if (Event.current == null)
        {
            return;
        }

        Vector2 mousePosition = new Vector2(Event.current.mousePosition.x, Event.current.mousePosition.y);


        Ray ray = HandleUtility.GUIPointToWorldRay(mousePosition);

        CurrentHandlePosition = GetTilePosition(ray.origin);

        CurrentHandlePosition.x += 0.5f * tileSizeMultiplier;
        CurrentHandlePosition.y -= 0.5f * tileSizeMultiplier;
        

    }

    static void UpdateRepaint()
    {
        //If the cube handle position has changed, repaint the scene
        if (CurrentHandlePosition != m_OldHandlePosition)
        {
            SceneView.RepaintAll();
            m_OldHandlePosition = CurrentHandlePosition;
        }
    }
}
