﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MSRoom))]
public class MSRoomEditor : Editor {

    private static Texture2D selectionBox;
    MSRoom room;
    Vector2 scrollview;
    Vector2 mousePos;
    float zoom = 1.5f;
    //int[] numberOfTiles = new int[2];
    int[] selectedStart = new int[2];   // The top-left-most tile selected
    int horizTilesSelected;   // How many tiles wide the selection is
    int vertTilesSelected;    // How many tiles tall
    bool isDragging;

    public override void OnInspectorGUI()
    {
        room = (MSRoom)target;
        selectionBox = (Texture2D)Resources.Load("selectionBox");

        if (Event.current.type == EventType.ValidateCommand && Event.current.commandName.Equals("UndoRedoPerformed"))
            room.GenerateTileLayer();

        GUILayout.Space(10);
        GUILayout.Label("MSRoomEditorPainter", EditorStyles.boldLabel);
        GUILayout.Space(5);
        room.levelAssets = EditorGUILayout.ObjectField("Level Assets", room.levelAssets, typeof(MSLevelAssets), false) as MSLevelAssets;
        EditorGUILayout.Space();
        GUILayout.Label("Nota: Si cambias de LevelAssets mientras estás creando una habitación se borrará");
        CheckLevelAssets();
        GUILayout.Space(10);
        Rect auxRect = EditorGUILayout.BeginHorizontal();
        MSDrawBox((int)auxRect.x, (int)auxRect.y, (int)auxRect.width, 2, Color.black);
        EditorGUILayout.EndHorizontal();
        GUILayout.Space(20);

        if (room.levelAssets != null)
        {
            //botones para controlar la acción a realizar

            auxRect = EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Tiles"))
                EditorPrefs.SetInt("MSOptionSelected", 1);
            if (GUILayout.Button("Collisions"))
                EditorPrefs.SetInt("MSOptionSelected", 2);
            if (GUILayout.Button("Prefabs"))
                EditorPrefs.SetInt("MSOptionSelected", 3);

            EditorGUILayout.EndHorizontal();
            GUILayout.Space(20);
            auxRect = EditorGUILayout.BeginHorizontal();
            MSDrawBox((int)auxRect.x, (int)auxRect.y, (int)auxRect.width, 2, Color.black);
            EditorGUILayout.EndHorizontal();
            GUILayout.Space(20);

            switch (EditorPrefs.GetInt("MSOptionSelected", 1))
            {
                case 1:
                    DrawTilePainterScreen();
                    break;
                case 2:
                    DrawColliderPainterScreen();
                    break;
                case 3:
                    break;
                default:
                    break;
            }

        }else
        {
            GUILayout.Space(30);
            GUIStyle style = new GUIStyle(GUI.skin.label)
            { alignment = TextAnchor.MiddleCenter, fontSize = 15, fontStyle = FontStyle.Bold, wordWrap = true};
            GUILayout.Label("Asigna un ScriptableObject MSLevelAssets para empezar a crear habitaciones", style, GUILayout.ExpandWidth(true));
            GUILayout.Space(40);
        }
        oldLevelAssets = room.levelAssets;
    }

    void DrawTilePainterScreen()
    {
        if (room.levelAssets.texture != null)
        {
            EditorGUILayout.Space();

            string[] sortingLayerNames = SortingLayer.layers.Select(l => l.name).ToArray();
            int[] sortingLayerNums = SortingLayer.layers.Select(l => l.id).ToArray();

            room.sortingLayerIndex = EditorGUILayout.IntPopup("Sorting Layer", room.sortingLayerIndex, sortingLayerNames, sortingLayerNums);

            GUILayout.Space(32);
            DrawTileSelector();
        }
        else
        {
            GUILayout.Space(30);
            GUIStyle style = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter, fontSize = 15, fontStyle = FontStyle.Bold, wordWrap = true };
            GUILayout.Label("El MSLevelAssets no contiene ningún tileset.", style, GUILayout.ExpandWidth(true));
            GUILayout.Space(40);
        }
    }

    void DrawColliderPainterScreen()
    {
        EditorGUILayout.Space();

        room.collisionsLayerIndex = EditorGUILayout.LayerField(room.collisionsLayerIndex);
        //Debug.Log(room.collisionsLayerIndex + " " + LayerMask.LayerToName(room.collisionsLayerIndex));

        GUILayout.Space(32);

    }

    void ActiveHandlerStuff(bool active)
    {
        EditorPrefs.SetInt("BasicHandleEnabled", 1);
        ActiveEditorTracker.sharedTracker.isLocked = active;
        Tools.hidden = active;

        room.GenerateTileLayer();
    }

    MSLevelAssets oldLevelAssets;

    /*
     Copyright 2010 CJ Currie
     @Date: October 2010
     @Contact: CJCurrie@BlackStormsStudios.com
     @Script: Creates the window that chooses the tiles and submits them to the TileManagerEditor.
     @Connections: 
     @TODO: TWEAK IT TO MAKE IT SUITABLE TO MY PROJECT
    */
    void DrawTileSelector()
    {
        zoom = EditorGUILayout.Slider("Zoom", zoom, 0.5f, 4f);
        EditorGUILayout.Space();

        scrollview = EditorGUILayout.BeginScrollView(scrollview);

        //Empty label trick to make the scrollview the size of the texture multiplied by the zoom
        GUILayout.Label("",
            GUILayout.Width(room.levelAssets.texture.width * zoom),
            GUILayout.Height(room.levelAssets.texture.height * zoom));
        GUI.DrawTexture(new Rect(0, 0, room.levelAssets.texture.width * zoom, room.levelAssets.texture.height * zoom), room.levelAssets.texture);


        Event e = Event.current;
        int[] currentPos = new int[2];
        currentPos[0] = (int)((e.mousePosition.x + room.levelAssets.offset.x) / (room.levelAssets.tileSize + room.levelAssets.padding.x) / zoom);
        currentPos[1] = (int)((e.mousePosition.y + room.levelAssets.offset.y) / (room.levelAssets.tileSize + room.levelAssets.padding.y) / zoom);
        
        bool hasSelected = false;

        if (e.type == EventType.MouseDown && e.button == 0)   // Left mouse button down - first selection
        {
            selectedStart[0] = currentPos[0];
            selectedStart[1] = currentPos[1];
            //Debug.Log(selectedStart[0] + " " + selectedStart[1]);
            horizTilesSelected = 0;
            vertTilesSelected = 0;

            isDragging = true;
        }
        else if (isDragging && e.type == EventType.MouseUp && e.button == 0)
        {
            hasSelected = true;
            isDragging = false;
        }
        
        
        GUI.DrawTexture(new Rect
            ((selectedStart[0]) * zoom * room.levelAssets.tileSize + zoom * room.levelAssets.offset.x + zoom * room.levelAssets.padding.x * (selectedStart[0]),
            (selectedStart[1]) * zoom * room.levelAssets.tileSize + zoom * room.levelAssets.offset.y + zoom * room.levelAssets.padding.y * (selectedStart[1]),
            room.levelAssets.tileSize * zoom * (horizTilesSelected + 1),
            room.levelAssets.tileSize * zoom * (vertTilesSelected + 1)),
            selectionBox);

        // End scroll view
        EditorGUILayout.EndScrollView();


        if (hasSelected && !isDragging)
        {
            int numSprite = (selectedStart[0]) + ((selectedStart[1]) * room.levelAssets.xSize);
            if (numSprite < room.levelAssets.tileSet.Length)
                room.spr = room.levelAssets.tileSet[numSprite];
            else
                room.spr = null;

            //Debug.Log((selectedStart[0] - offset[0]) + " " + (selectedStart[1] - offset[1]));
            //Debug.Log("ES EL SPRITE NÚMERO " + numSprite);
        }
    }
    
    void CheckLevelAssets()
    {
        if (oldLevelAssets != room.levelAssets)
        {
            Debug.Log("HA CAMBIADO DE LEVELASSETS, PERO CHEQUEAR ESTO BIEN Y TAL PARA VER QUE TODO SE HACE BIEN");
            //room.ResetRoom();
            if (room.levelAssets != null)
            {
                selectedStart[0] = 0;
                selectedStart[1] = 0;
                room.spr = room.levelAssets.tileSet[0];
            }
        }
    }

    void MSDrawBox(int x, int y, int width, int height, Color color)
    {
        Rect rect = new Rect(x, y, width, height);
        Texture2D texture = new Texture2D(1, 1);
        texture.SetPixel(0, 0, color);
        texture.Apply();
        GUIStyle style = new GUIStyle();
        style.normal.background = texture;
        GUI.Box(rect, GUIContent.none, style);
    }
    
}
