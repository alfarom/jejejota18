﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MSPainter/LevelAssets")]
public class MSLevelAssets : ScriptableObject
{
    public Sprite[] tileSet;
    public GameObject[] prefabSet;
    public Texture2D texture;
    public int xSize;
    public int ySize;
    public int tileSize;
    public Vector2 offset;
    public Vector2 padding;
}

