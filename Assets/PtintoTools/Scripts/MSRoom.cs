﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MSTile
{
    public Vector2 position;
    public GameObject tileObject;
    public SpriteRenderer spriteRenderer;

    public MSTile(Vector2 pos, GameObject tile)
    {
        position = pos;
        tileObject = tile;
        spriteRenderer = tile.GetComponent<SpriteRenderer>();
    }
}
public class MSCollider
{
    public Vector2 position;
    public GameObject gameObject;

    public MSCollider(Vector2 pos, GameObject obj)
    {
        position = pos;
        gameObject = obj;
    }

}
public class MSPosition
{
    public float x;
    public float y;

    public MSPosition(float posX, float posY)
    {
        x = posX;
        y = posY;
    }
    public MSPosition(Vector2 vec)
    {
        x = vec.x;
        y = vec.y;
    }
}
public class MSPositionKeyComparer : IEqualityComparer<MSPosition>
{
    public bool Equals(MSPosition vec1, MSPosition vec2)
    {
        //Debug.Log("COMPARANDO UNA KEY  (" + vec1.x + ", " + vec1.y + ")   (" + vec2.x + ", " + vec2.y + ") ");
        if (vec1.x != vec2.x)
        {
            //Debug.Log("falso en x " + vec1.x + " " + vec2.x);
            return false;
        }
        if (vec1.y != vec2.y)
        {
            //Debug.Log("falso en y " + vec1.y + " " + vec2.y);
            return false;
        }
        return true;
    }

    public int GetHashCode(MSPosition obj)
    {
        int result = 17;
        unchecked
        {
            result = result * 23 + obj.x.GetHashCode();
            result = result * 23 + obj.y.GetHashCode();
        }
        //Debug.Log("hashcode check (" + obj.x + ", " + obj.y + ") " + result);
        return result;
    }
}
public class MSRoom : MonoBehaviour
{
    private Dictionary<int, Dictionary<MSPosition, MSCollider>> colliderLayers;
    private Dictionary<int, Dictionary<MSPosition, MSTile>> tileLayers;
    private Dictionary<MSPosition, MSTile> paintedTiles;

    public MSLevelAssets levelAssets;
    public UnityEngine.Object spr;
    public int sortingLayerIndex = 0;
    public int collisionsLayerIndex;


#if (UNITY_EDITOR)

    public void GenerateTileLayer()
    {
        //Debug.Log("regenerando tiles de la habitacion");
        //ClearChildren();
        if (tileLayers == null)
        {
            //Debug.Log("NULL");
            tileLayers = new Dictionary<int, Dictionary<MSPosition, MSTile>>();
            foreach (Transform child in transform)
            {
                if (SortingLayer.NameToID(child.name) != 0)
                {
                    tileLayers.Add(SortingLayer.NameToID(child.name), new Dictionary<MSPosition, MSTile>(new MSPositionKeyComparer()));
                    foreach(Transform grandChild in child)
                    {
                        tileLayers[SortingLayer.NameToID(child.name)].Add(new MSPosition(grandChild.position), new MSTile(grandChild.position, grandChild.gameObject));
                    }
                }
            }
        }
        else
        {
            foreach(KeyValuePair<int, Dictionary<MSPosition, MSTile>> entry in tileLayers)
            {
                entry.Value.Clear();
            }
            tileLayers.Clear();
            foreach (Transform child in transform)
            {
                if (SortingLayer.NameToID(child.name) != 0)
                {
                    tileLayers.Add(SortingLayer.NameToID(child.name), new Dictionary<MSPosition, MSTile>(new MSPositionKeyComparer()));
                    foreach (Transform grandChild in child)
                    {
                        tileLayers[SortingLayer.NameToID(child.name)].Add(new MSPosition(grandChild.position), new MSTile(grandChild.position, grandChild.gameObject));
                    }
                }
            }
        }
    }

    public void ClearChildren()
    {
        List<Transform> childrenToClear = new List<Transform>();
        SpriteRenderer auxSpriteRenderer;
        foreach (Transform child in transform)
        {
            auxSpriteRenderer = child.GetComponent<SpriteRenderer>();
            if (auxSpriteRenderer != null)
            {
                if (auxSpriteRenderer.sprite == null)
                    childrenToClear.Add(child);
            }
        }
        foreach (Transform c in childrenToClear)
        {
            DestroyImmediate(c.gameObject);
        }
    }

    bool CheckForSpace(MSPosition position)
    {
        //SI NO HAY NADA CON ESTA KEY, HAY UN ESPACIO -> TRUE
        return !tileLayers[sortingLayerIndex].ContainsKey(position);
    }

    bool CheckForColliderSpace(MSPosition position)
    {
        //SI NO HAY NADA CON ESTA KEY, HAY UN ESPACIO -> TRUE
        return !colliderLayers[collisionsLayerIndex].ContainsKey(position);
    }

    public void PaintTile(Vector2 position)
    {
        if(spr != null)
        {
            Transform sortingLayerTransf = FindSortingLayerTransform();
            MSPosition tilePosition = new MSPosition(position);
            if (CheckForSpace(tilePosition))
            {
                GameObject ob = new GameObject("Tile");
                //ob.hideFlags = HideFlags.HideInHierarchy;
                Vector3 tilePos = position;
                ob.transform.position = tilePos;
                ob.transform.SetParent(sortingLayerTransf);
                ob.AddComponent(typeof(SpriteRenderer));
                ob.GetComponent<SpriteRenderer>().sprite = spr as Sprite;
                ob.GetComponent<SpriteRenderer>().sortingLayerID = sortingLayerIndex;
                UnityEditor.Undo.RegisterCreatedObjectUndo(ob, "Paint Tile");
                UnityEditor.SceneManagement.EditorSceneManager.MarkAllScenesDirty();
                MSTile tileToCreate = new MSTile(position, ob);
                tileLayers[sortingLayerIndex].Add(tilePosition, tileToCreate);
            }
            else
            {
                MSTile tileToRepaint = tileLayers[sortingLayerIndex][tilePosition];
                if (tileToRepaint != null && tileToRepaint.spriteRenderer != null)
                {
                    UnityEditor.Undo.RecordObject(tileToRepaint.spriteRenderer, "Paint Tile");
                    tileToRepaint.spriteRenderer.sprite = spr as Sprite;
                }
            }
        }
    }

    Transform FindSortingLayerTransform()
    {
        if (tileLayers == null)
            tileLayers = new Dictionary<int, Dictionary<MSPosition, MSTile>>();

        if (!tileLayers.ContainsKey(sortingLayerIndex))
        {
            tileLayers.Add(sortingLayerIndex, new Dictionary<MSPosition, MSTile>(new MSPositionKeyComparer()));
        }

        Transform tilesTransform = transform.Find("Tiles");

        if(tilesTransform == null)
        {
            GameObject tilesObj = new GameObject("Tiles");
            tilesObj.transform.SetParent(transform);
            tilesObj.transform.localPosition = Vector3.zero;
            tilesTransform = tilesObj.transform;
        }

        GameObject layerObj = null;

        if (tilesTransform.Find(SortingLayer.IDToName(sortingLayerIndex)) == null)
        {
            layerObj = new GameObject(SortingLayer.IDToName(sortingLayerIndex));
            layerObj.transform.SetParent(tilesTransform);
            layerObj.transform.localPosition = Vector3.zero;
        }

        if(layerObj == null)
            layerObj = tilesTransform.Find(SortingLayer.IDToName(sortingLayerIndex)).gameObject;
        
        return layerObj.transform;
    }

    public void PaintCollider(Vector2 position)
    {
        Transform colliderLayerTransform = FindColliderLayerTransform();
        MSPosition tilePosition = new MSPosition(position);
        if (CheckForColliderSpace(tilePosition))
        {
            GameObject ob = new GameObject("Collider");
            //ob.hideFlags = HideFlags.HideInHierarchy;
            Vector3 colliderPos = position;
            ob.transform.position = colliderPos;
            ob.transform.SetParent(colliderLayerTransform);
            ob.AddComponent(typeof(BoxCollider2D));
            Vector2 spriteBoundsSize = (spr as Sprite).bounds.size;
            ob.GetComponent<BoxCollider2D>().offset = new Vector2(0.5f * spriteBoundsSize.x, -0.5f * spriteBoundsSize.y);
            ob.GetComponent<BoxCollider2D>().size = spriteBoundsSize;
            ob.layer = collisionsLayerIndex;
            UnityEditor.Undo.RegisterCreatedObjectUndo(ob, "Create Collider");
            UnityEditor.SceneManagement.EditorSceneManager.MarkAllScenesDirty();
            MSCollider colliderToCreate = new MSCollider(position, ob);
            colliderLayers[collisionsLayerIndex].Add(tilePosition, colliderToCreate);
        }

    }

    Transform FindColliderLayerTransform()
    {
        if (colliderLayers == null)
            colliderLayers = new Dictionary<int, Dictionary<MSPosition, MSCollider>>();

        if (!colliderLayers.ContainsKey(collisionsLayerIndex))
        {
            colliderLayers.Add(collisionsLayerIndex, new Dictionary<MSPosition, MSCollider>(new MSPositionKeyComparer()));
        }

        Transform collidersTransform = transform.Find("Colliders");

        if (collidersTransform == null)
        {
            GameObject obj = new GameObject("Colliders");
            obj.transform.SetParent(transform);
            obj.transform.localPosition = Vector3.zero;
            collidersTransform = obj.transform;
        }

        GameObject layerObj = null;

        if (collidersTransform.Find(LayerMask.LayerToName(collisionsLayerIndex)) == null)
        {
            layerObj = new GameObject(LayerMask.LayerToName(collisionsLayerIndex));
            layerObj.transform.SetParent(collidersTransform);
            layerObj.transform.localPosition = Vector3.zero;
        }

        if (layerObj == null)
            layerObj = collidersTransform.Find(LayerMask.LayerToName(collisionsLayerIndex)).gameObject;

        return layerObj.transform;
    }

    public void EraseTile(Vector2 position)
    {
        MSPosition tilePosition = new MSPosition(position);
        if (!CheckForSpace(tilePosition))
        {
            MSTile tileToRepaint = tileLayers[sortingLayerIndex][tilePosition];
            if (tileToRepaint != null && tileToRepaint.spriteRenderer != null)
            {
                UnityEditor.Undo.RecordObject(tileToRepaint.spriteRenderer, "Erase Tile");
                tileToRepaint.spriteRenderer.sprite = null;
            }
        }
    }

    public void EraseCollider(Vector2 position)
    {
        MSPosition tilePosition = new MSPosition(position);
        if (!CheckForColliderSpace(tilePosition))
        {
            MSCollider colliderToDestroy = colliderLayers[collisionsLayerIndex][tilePosition];
            UnityEditor.Undo.RegisterCreatedObjectUndo(colliderToDestroy.gameObject, "Create Collider");
            UnityEditor.SceneManagement.EditorSceneManager.MarkAllScenesDirty();
            DestroyImmediate(colliderToDestroy.gameObject);
            colliderLayers[collisionsLayerIndex].Remove(tilePosition);
        }
    }

    public void ResetRoom()
    {
        if (tileLayers != null)
        {
            foreach (KeyValuePair<int, Dictionary<MSPosition, MSTile>> entry in tileLayers)
            {
                entry.Value.Clear();
            }
            tileLayers.Clear();
        }
        if (colliderLayers != null)
        {
            foreach (KeyValuePair<int, Dictionary<MSPosition, MSCollider>> entry in colliderLayers)
            {
                entry.Value.Clear();
            }
            colliderLayers.Clear();
        }
        DestroyChildren(gameObject);
    }

    public void DestroyChildren(GameObject go)
    {
        List<GameObject> children = new List<GameObject>();
        foreach (Transform tran in go.transform)
        {
            children.Add(tran.gameObject);
        }
        children.ForEach(child => DestroyImmediate(child));
    }
#endif

}
