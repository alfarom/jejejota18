﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserComponent : MonoBehaviour {

    public LaserDirection laserDirection;
    public GameObject laserHitPrefab;
    public LayerMask ignoreLayers;
    private GameObject laserHit;
    private LineRenderer lineRenderer;
    private AudioSource audioSource;

	// Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
        lineRenderer = GetComponent<LineRenderer>();
     }
	
	// Update is called once per frame
	void Update () {
        RaycastHit2D hit;
        if ((hit = Physics2D.Raycast(transform.position, GetLaserDirection(), int.MaxValue, ignoreLayers)))
        {
            if (!hit.transform.CompareTag("Player") && !hit.transform.CompareTag("Player2") && !hit.transform.CompareTag("Box"))
            {
                lineRenderer.SetPosition(0, transform.position);
                lineRenderer.SetPosition(1, hit.point);
                audioSource.Stop();
                if (laserHit != null) Destroy(laserHit);
            }
            else if (hit.transform.CompareTag("Player2") || hit.transform.CompareTag("Box"))
            {
                lineRenderer.SetPosition(0, transform.position);
                lineRenderer.SetPosition(1, hit.point);
                if (laserHit == null)
                {
                    laserHit = Instantiate(laserHitPrefab, hit.transform);
                    laserHit.transform.position = hit.point;
                    audioSource.Play();
                }
                else
                {
                    laserHit.transform.position = hit.point;
                }
            }
            else
            {
                if ((hit = Physics2D.Raycast(transform.position, GetLaserDirection(), int.MaxValue, ignoreLayers)))
                {
                    if (hit.transform.CompareTag("Player"))
                    {
                        
                        //Empujar al player
                        hit.transform.GetComponent<ChispasController>().Push(hit.point);
                    }
                }
            }
        }
    }

    public Vector3 GetLaserDirection()
    {
        switch (laserDirection)
        {
            case LaserDirection.UP:
                return transform.up;
            case LaserDirection.DOWN:
                return -transform.up;
            case LaserDirection.LEFT:
                return -transform.right;
            case LaserDirection.RIGHT:
                return transform.right;
        }
        return Vector3.zero;
    }
}

public enum LaserDirection
{
    UP, DOWN, LEFT, RIGHT
}
