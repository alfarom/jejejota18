﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadComponent : MonoBehaviour {

    public SpriteRenderer headSprite;
    public Sprite[] headSprites;

    private void Awake()
    {
        headSprite = GetComponent<SpriteRenderer>();
    }

    public void SetMovement(float horizontal, float vertical)
    {
       if(horizontal==0 && vertical == 0)
        {
            headSprite.sprite = headSprites[0];
        }else if (horizontal > 0)
        {
            headSprite.sprite = headSprites[1];
        }
        else if (horizontal < 0)
        {
            headSprite.sprite = headSprites[2];
        }
        else if (vertical > 0)
        {
            headSprite.sprite = headSprites[3];
        }
        else if (vertical < 0)
        {
            headSprite.sprite = headSprites[0];
        }
    }

    public void Active(bool active)
    {
        if (active)
            headSprite.color = Color.white;
        else
            headSprite.color = Color.gray;
    }

}
