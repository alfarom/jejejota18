﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotController : PlayerController {

    private Transform grabObject;
    [SerializeField]
    private LayerMask grabLayer;

    private new void Start()
    {
        active = true;
        base.Start();
    }

    public override void RegisterControllers()
    {
        GameMgr.Instance.GetServer<InputMgr>().GetInputPlayerController(tag).RegisterMoveEvent(Move);
        GameMgr.Instance.GetServer<InputMgr>().GetInputPlayerController(tag).RegisterPresionedActionEvent(Action);
    }

    public override void UnRegisterControllers()
    {
        GameMgr.Instance.GetServer<InputMgr>().GetInputPlayerController(tag).UnRegisterMoveEvent(Move);
        GameMgr.Instance.GetServer<InputMgr>().GetInputPlayerController(tag).UnPresionedRegisterActionEvent(Action);
        m_rigidbody.velocity = Vector2.zero;
    }

    public void Action(float horizontal, float vertical, bool grab)
    {
        //Si !grab es que hay que soltar el objeto
        if (!grab && grabObject)
        {
            grabObject.parent = null;
            grabObject = null;
            return;
        }

        if (grabObject) return;

        RaycastHit2D hit;
        Vector3 initialPositionRight = transform.Find("GrabCollider").position + (transform.right * GetComponent<BoxCollider2D>().size.x / 3);
        Vector3 initialPositionRight1 = transform.Find("GrabCollider").position + (transform.up * GetComponent<BoxCollider2D>().size.y / 3) + (transform.right * GetComponent<BoxCollider2D>().size.x / 2);
        Vector3 initialPositionRight2 = transform.Find("GrabCollider").position - (transform.up * GetComponent<BoxCollider2D>().size.y / 3) + (transform.right * GetComponent<BoxCollider2D>().size.x / 2);

        Vector3 initialPositionUp = transform.Find("GrabCollider").position + (transform.up * GetComponent<BoxCollider2D>().size.y / 3);
        Vector3 initialPositionUp1 = transform.Find("GrabCollider").position + (transform.right * GetComponent<BoxCollider2D>().size.x / 3) + (transform.up * GetComponent<BoxCollider2D>().size.y / 2);
        Vector3 initialPositionUp2 = transform.Find("GrabCollider").position - (transform.right * GetComponent<BoxCollider2D>().size.x / 3) + (transform.up * GetComponent<BoxCollider2D>().size.y / 2);


        Vector3 initialPositionLeft = transform.Find("GrabCollider").position - (transform.right * GetComponent<BoxCollider2D>().size.x / 3);
        Vector3 initialPositionLeft1 = transform.Find("GrabCollider").position + (transform.up * GetComponent<BoxCollider2D>().size.y / 3) - (transform.right * GetComponent<BoxCollider2D>().size.x / 2);
        Vector3 initialPositionLeft2 = transform.Find("GrabCollider").position - (transform.up * GetComponent<BoxCollider2D>().size.y / 3) - (transform.right * GetComponent<BoxCollider2D>().size.x / 2);


        Vector3 initialPositionDown = transform.Find("GrabCollider").position - (transform.up * GetComponent<BoxCollider2D>().size.y / 3);
        Vector3 initialPositionDown1 = transform.Find("GrabCollider").position + (transform.right * GetComponent<BoxCollider2D>().size.x / 3) - (transform.up * GetComponent<BoxCollider2D>().size.y / 2);
        Vector3 initialPositionDown2 = transform.Find("GrabCollider").position - (transform.right * GetComponent<BoxCollider2D>().size.x / 3) - (transform.up * GetComponent<BoxCollider2D>().size.y / 2);


        //Si al hacer la accion el raycast colisiona con algo de la layer 'Grab'
        //Lo cogemos y lo movemos
        if ((hit = Physics2D.Raycast(initialPositionRight, transform.right, 1.5f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionRight1, transform.right, 1.5f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionRight2, transform.right, 1.5f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionUp, transform.up, 2.5f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionUp1, transform.up, 2.5f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionUp2, transform.up, 2.5f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionLeft, -transform.right, 1.5f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionLeft1, -transform.right, 1.5f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionLeft2, -transform.right, 1.5f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionDown, -transform.up, 1f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionDown1, -transform.up, 1f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionDown2, -transform.up, 1f, grabLayer))
            )
        {
            GrabObject(hit);

        }

    }

    private void GrabObject(RaycastHit2D hit)
    {
        if (hit.transform == transform) return;
        grabObject = hit.transform;
        grabObject.parent = transform;
    }
    

    public override void Action(float horizontal, float vertical)
    {

    }

}
