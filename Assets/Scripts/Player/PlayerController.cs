﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(InputPlayerController))]
public abstract class PlayerController : MonoBehaviour {

    protected Rigidbody2D m_rigidbody;
    protected SpriteRenderer spriteRenderer;
    private HeadComponent headComponent;
    public bool active;
    public bool stunned;

    [SerializeField]
    protected float speed;

    public void Start()
    {
        GameMgr.Instance.GetServer<InputMgr>().AddInputPlayerController(tag, GetComponent<InputPlayerController>());

        //Inicializamos los componentes
        m_rigidbody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        headComponent = spriteRenderer.GetComponentInChildren<HeadComponent>();

        //Si esta activo registramos los controles
        if (active)
            RegisterControllers();

        SetPlayerActive(active);
        m_rigidbody.isKinematic = !active;

        //Registramos el evento de cambiar jugador
        GameMgr.Instance.GetServer<InputMgr>().GetInputPlayerController(tag).RegisterChangePlayerEvent(ChangePlayer);
    }

    /**
     * Método del evento para el movimiento del player
     * */
    public void Move(float horizontal, float vertical)
    {
        if (stunned) return;
        Vector2 move = transform.right * horizontal + transform.up * vertical;
        Vector2 movement = move * speed;
        m_rigidbody.velocity=movement;
        headComponent.SetMovement(horizontal, vertical);
    }

    /**
     * Método del eventopara el cambio de personaje
     * */
    public virtual void ChangePlayer()
    {
        if (active)
        {
            UnRegisterControllers();
        }
        else
        {
            RegisterControllers();
        }
        active = !active;
        SetPlayerActive(active);
       
        m_rigidbody.isKinematic = !active;
    }

    public void SetPlayerActive(bool active)
    {
        if (active)
            spriteRenderer.color = Color.white;
        else
            spriteRenderer.color = Color.gray;

        headComponent.Active(active);
    }

    /**
     * Método del evento para la acción del player
     * */
    public abstract void Action(float horizontal, float vertical);

    public abstract void RegisterControllers();

    public abstract void UnRegisterControllers();

    private void OnDestroy()
    {
        if (GameMgr.Instance != null)
        {
            UnRegisterControllers();
            GameMgr.Instance.GetServer<InputMgr>().GetInputPlayerController(tag).UnRegisterChangePlayerEvent(ChangePlayer);
            GameMgr.Instance.GetServer<InputMgr>().RemoveInputPlayerController(tag);
        }
    }

}
