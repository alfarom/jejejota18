﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChispasController : PlayerController
{

    [SerializeField]
    private LayerMask grabLayer;

    [SerializeField]
    private Cable cableObject;

    private Coroutine stunCoroutine;

    public BoxCollider2D collider;
    

    private new void Start()
    {
        active = true;
        cableObject = null;
        base.Start();

    }

    public override void RegisterControllers()
    {
        GameMgr.Instance.GetServer<InputMgr>().GetInputPlayerController(tag).RegisterMoveEvent(Move);
        GameMgr.Instance.GetServer<InputMgr>().GetInputPlayerController(tag).RegisterActionEvent(Action);
    }

    public override void UnRegisterControllers()
    {
        GameMgr.Instance.GetServer<InputMgr>().GetInputPlayerController(tag).UnRegisterMoveEvent(Move);
        GameMgr.Instance.GetServer<InputMgr>().GetInputPlayerController(tag).UnRegisterActionEvent(Action);
        m_rigidbody.velocity = Vector2.zero;
    }

    void Update()
    {
        if(cableObject != null)
        {
            cableObject.MoveCable(transform.position);
        }
    }

    public override void Action(float horizontal, float vertical)
    {
        RaycastHit2D hit;
        Vector3 initialPositionRight = transform.Find("GrabCollider").position + (transform.right * GetComponent<BoxCollider2D>().size.x / 3);
        Vector3 initialPositionRight1 = transform.Find("GrabCollider").position + (transform.up * GetComponent<BoxCollider2D>().size.y / 3) + (transform.right * GetComponent<BoxCollider2D>().size.x / 2);
        Vector3 initialPositionRight2 = transform.Find("GrabCollider").position - (transform.up * GetComponent<BoxCollider2D>().size.y / 3) + (transform.right * GetComponent<BoxCollider2D>().size.x / 2);

        Vector3 initialPositionUp = transform.Find("GrabCollider").position + (transform.up * GetComponent<BoxCollider2D>().size.y / 3);
        Vector3 initialPositionUp1 = transform.Find("GrabCollider").position + (transform.right * GetComponent<BoxCollider2D>().size.x / 3) + (transform.up * GetComponent<BoxCollider2D>().size.y / 2);
        Vector3 initialPositionUp2 = transform.Find("GrabCollider").position - (transform.right * GetComponent<BoxCollider2D>().size.x / 3) + (transform.up * GetComponent<BoxCollider2D>().size.y / 2);


        Vector3 initialPositionLeft = transform.Find("GrabCollider").position - (transform.right * GetComponent<BoxCollider2D>().size.x / 3);
        Vector3 initialPositionLeft1 = transform.Find("GrabCollider").position + (transform.up * GetComponent<BoxCollider2D>().size.y / 3) - (transform.right * GetComponent<BoxCollider2D>().size.x / 2);
        Vector3 initialPositionLeft2 = transform.Find("GrabCollider").position - (transform.up * GetComponent<BoxCollider2D>().size.y / 3) - (transform.right * GetComponent<BoxCollider2D>().size.x / 2);


        Vector3 initialPositionDown = transform.Find("GrabCollider").position - (transform.up * GetComponent<BoxCollider2D>().size.y / 3);
        Vector3 initialPositionDown1 = transform.Find("GrabCollider").position + (transform.right * GetComponent<BoxCollider2D>().size.x / 3) - (transform.up * GetComponent<BoxCollider2D>().size.y / 2);
        Vector3 initialPositionDown2 = transform.Find("GrabCollider").position - (transform.right * GetComponent<BoxCollider2D>().size.x / 3) - (transform.up * GetComponent<BoxCollider2D>().size.y / 2);

        //Si al hacer la accion el raycast colisiona con algo de la layer 'Grab'
        //Lo cogemos y lo movemos
        if ((hit = Physics2D.Raycast(initialPositionRight, transform.right, 1.5f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionRight1, transform.right, 1.5f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionRight2, transform.right, 1.5f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionUp, transform.up, 2.5f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionUp1, transform.up, 2.5f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionUp2, transform.up, 2.5f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionLeft, -transform.right, 1.5f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionLeft1, -transform.right, 1.5f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionLeft2, -transform.right, 1.5f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionDown, -transform.up, 1f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionDown1, -transform.up, 1f, grabLayer))
            || (hit = Physics2D.Raycast(initialPositionDown2, -transform.up, 1f, grabLayer))
            )
        {
            //Si no tenemos ningun cable pedimos crearlo
            if (cableObject == null)
            {
                //Si estamos cogiendo un cable conectado a un input
                //Desconectamos el cable del input
                InputCable inputCable = hit.collider.GetComponent<InputCable>();
                if (inputCable)
                {
                    //Si no tiene cable conectado salimos del metodo
                    if (inputCable.GetCable() == null) return;

                    //Desconectamos el cable del input
                    inputCable.GetCable().DisconnectCable();
                    
                    //Guardamos la variable del cable
                    cableObject = inputCable.GetCable();
                    return;
                }


                //Si el cable lo cogemos desde un output creamos el cable
                cableObject = hit.collider.GetComponent<OutputCable>().GrabCable(transform);
            }
            else
            {

                //Si ya tenemos un cable comprobamos si lo estamos dejando en un input
                InputCable inputCable = hit.collider.GetComponent<InputCable>();

                //Si no lo dejamos en un input lo eliminamos
                if (!inputCable)
                {
                    cableObject.DropCable();
                    cableObject = null;
                    return;
                }

                //Sino conectamos el cable y eliminamos la referencia
                cableObject.ConnectCable(hit.collider.GetComponent<InputCable>());
                cableObject = null;
            }
        }
        else
        {
            //Si no lo estamo conectando a ningun lado eliminamos el objeto
            if(cableObject != null)
                cableObject.DropCable();
            cableObject = null;
            return;
        }

    }
    public void ReleaseCable()
    {
        cableObject.DropCable();
        cableObject = null;
    }

    public void Push(Vector2 velocity)
    {
        if(stunCoroutine!=null)
            StopCoroutine(stunCoroutine);
        stunCoroutine = StartCoroutine(Stunned(velocity));
    }

    IEnumerator Stunned(Vector2 velocity)
    {
        stunned = true;
        float timer = 0.0f;
        float stunnedTime = 0.05f;

        m_rigidbody.velocity = Vector2.zero;
        velocity = transform.Find("GrabCollider").position - (Vector3)velocity;

        while (timer <= stunnedTime)
        {
            timer += Time.deltaTime;
            Vector3 movement = velocity.normalized * speed;
            m_rigidbody.velocity = movement;
            yield return null;
        }

        stunned = false;
    }

    /**
     * Método del eventopara el cambio de personaje
     * */
    public override void ChangePlayer()
    {
        base.ChangePlayer();
        stunned = false;
        m_rigidbody.velocity = Vector2.zero;
    }

}
