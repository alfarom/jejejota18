﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputPlayerController : MonoBehaviour {

    public delegate void MoveEvent(float horizontal, float vertical);
    public delegate void ActionEvent(float horizontal, float vertical);
    public delegate void PresionedActionEvent(float horizontal, float vertical, bool grab);
    public delegate void ChangePlayerEvent();

    public MoveEvent m_movement;
    protected ActionEvent m_action;
    protected PresionedActionEvent m_presionedAction;
    protected ChangePlayerEvent m_changePlayer;

    //Registramos los eventos     
    public void RegisterMoveEvent(MoveEvent movement)
    {
        if (movement != null)
        {
            m_movement += movement;
        }
    }
    //Desregistro de eventos.
    public void UnRegisterMoveEvent(MoveEvent movement)
    {
        if (movement != null)
        {
            m_movement -= movement;
        }
    }

    //Registramos los eventos 
    public void RegisterActionEvent(ActionEvent actionEvent)
    {
        if (actionEvent != null)
        {
            m_action += actionEvent;
        }
    }
    //Desregistro de eventos
    public void UnRegisterActionEvent(ActionEvent actionEvent)
    {
        if (actionEvent != null)
        {
            m_action -= actionEvent;
        }
    }


    //Registramos los eventos 
    public void RegisterPresionedActionEvent(PresionedActionEvent presionedActionEvent)
    {
        if (presionedActionEvent != null)
        {
            m_presionedAction += presionedActionEvent;
        }
    }
    //Desregistro de eventos
    public void UnPresionedRegisterActionEvent(PresionedActionEvent presionedActionEvent)
    {
        if (presionedActionEvent != null)
        {
            m_presionedAction -= presionedActionEvent;
        }
    }

    //Registramos los eventos 
    public void RegisterChangePlayerEvent(ChangePlayerEvent changePlayer)
    {
        if (changePlayer != null)
        {
            m_changePlayer += changePlayer;
        }
    }
    //Desregistro de eventos
    public void UnRegisterChangePlayerEvent(ChangePlayerEvent changePlayer)
    {
        if (changePlayer != null)
        {
            m_changePlayer -= changePlayer;
        }
    }


    // Update is called once per frame
    void Update()
    {
        //Call attack function
        if (Input.GetButtonDown(fire) && m_action != null)
            m_action(Input.GetAxis(horizontal), Input.GetAxis(vertical));

        if (Input.GetButtonUp(fire) && m_presionedAction != null)
            m_presionedAction(Input.GetAxis(horizontal), Input.GetAxis(vertical), false);
        else if (Input.GetButton(fire) && m_presionedAction != null)
            m_presionedAction(Input.GetAxis(horizontal), Input.GetAxis(vertical), true);

        //if (Input.GetButtonDown("ChangePlayer"))
        //    m_changePlayer();

        //Call movement function
        if (m_movement != null)
            m_movement(Input.GetAxis(horizontal), Input.GetAxis(vertical));

    }

    public string horizontal, vertical, fire;
}
