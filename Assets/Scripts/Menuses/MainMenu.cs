﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    Animator anim;
    AudioSource audioSource;
    bool gameStarted = false;

    void Start()
    {
        anim = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

	// Update is called once per frame
	void Update ()
    {
	    if(Input.anyKeyDown && !gameStarted)
        {
            gameStarted = true;
            anim.SetTrigger("StartGame");
            audioSource.Play();
            StartCoroutine(StartGameCoroutine());
        }	
	}

    IEnumerator StartGameCoroutine()
    {
        yield return new WaitForSeconds(2f);

        SceneManager.LoadScene("Level1");
    }
}
