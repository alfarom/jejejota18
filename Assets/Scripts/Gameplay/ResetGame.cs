﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetGame : MonoBehaviour {

	void Update () {
        if (Input.GetKeyDown(KeyCode.F1) || Input.GetButtonDown("Reset"))
        {
            GameMgr.Instance.GetServer<SceneMgr>().ChangeScene("MainMenu");
        }
	}
}
