﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutputCable : MonoBehaviour {

    [SerializeField]
    GameObject cablePrefab;

    [SerializeField]
    EnergyNode parentNode;

    [SerializeField]
    Cable cable;

    [SerializeField]
    Sprite cableIn, cableOut, cableConnected;

    [SerializeField]
    SpriteRenderer spriteRenderer;

    public float distance;
    
    AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        GameObject cableObj = Instantiate(cablePrefab, transform);
        cable = cableObj.GetComponent<Cable>();
        cable.CreateCable(this, distance);
    }

    public void SetOutput(Cable cableToSet)
    { 
        cable = cableToSet;

    }

    public Cable GrabCable(Transform parent)
    {
        spriteRenderer.sprite = cableOut;

        if (cable.ending != null) { 
            cable.ending.DesactivateParentNode();
            cable.ending = null;
        }

        audioSource.Play();

        cable.GrabCable();
        return cable;
    }

    public void DropCable()
    {
        spriteRenderer.sprite = cableIn;
    }

    public void CableConnected()
    {
        spriteRenderer.sprite = cableConnected;
    }
}
