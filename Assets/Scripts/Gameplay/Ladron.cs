﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladron : EnergyNode
{
    public override void OnActionActive()
    {
        nodeSprite.color = Color.white;
    }

    public override void OnActionDesactive()
    {
        nodeSprite.color = Color.red;
    }
}
