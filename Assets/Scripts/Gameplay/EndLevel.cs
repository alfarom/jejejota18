﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EndLevel : MonoBehaviour {

    public Image fade;
    public float totalTime= 123;
    public CounterTime counterTime;

    private void Start()
    {
        if(counterTime.timer<0)
            StartCoroutine(ResetGame());
        else
            StartCoroutine(EndGame());
    }

    IEnumerator ResetGame()
    {
        GetComponent<AudioSource>().Play();
        fade.gameObject.SetActive(true);
        float timer = 0.0f;
        while (timer < 1.0f)
        {
            timer += Time.deltaTime/2;
            fade.color = Color.Lerp(Color.clear, Color.black, timer);
            yield return null;
        }
        GameMgr.Instance.GetServer<SceneMgr>().ChangeScene("Level1");
    }

    IEnumerator EndGame()
    {
        fade.gameObject.SetActive(true);
        float timer = 0.0f;
        while (timer < 1.0f)
        {
            timer += Time.deltaTime / 2;
            fade.color = Color.Lerp(Color.clear, Color.black, timer);
            yield return null;
        }
        GameMgr.Instance.GetServer<SceneMgr>().ChangeScene("EndGame");
    }
}
