﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EndGame : MonoBehaviour {

    public SpriteRenderer fade;

    private void Start()
    {
        StartCoroutine(ResetGame());
    }

    IEnumerator ResetGame()
    {
        Color initialColor = fade.color;
        float timer = 0.0f;
        yield return new WaitForSeconds(3);
        while (timer < 1.0f)
        {
            timer += Time.deltaTime/2;
            fade.color = Color.Lerp(initialColor, Color.black, timer);
            yield return null;
        }
        GameMgr.Instance.GetServer<SceneMgr>().ChangeScene("MainMenu");
    }
}
