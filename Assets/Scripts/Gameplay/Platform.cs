﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : EnergyNode {

    public GameObject[] gaps;

    public override void OnActionActive()
    {
        foreach (GameObject go in gaps)
        {
            go.SetActive(false);
        }

        gameObject.SetActive(false);
    }

    public override void OnActionDesactive()
    {
        foreach (GameObject go in gaps)
        {
            go.SetActive(true);
        }
        gameObject.SetActive(true);
    }
}
