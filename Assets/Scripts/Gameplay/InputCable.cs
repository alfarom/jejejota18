﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputCable : MonoBehaviour {

    [SerializeField]
    protected Cable cable;

    protected ActiveAction m_activeAction;
    protected DesactiveAction m_desactiveAction;

    public Sprite cableOut, cableConnected;
    public AudioClip plug, unplug;

    AudioSource audioSource;

    #region Metodos de los delegados
    //Registramos los eventos     
    public void RegisterActiveActionEvent(ActiveAction activeAction)
    {
        if (activeAction != null)
        {
            m_activeAction += activeAction;
        }
    }
    //Desregistro de eventos.
    public void UnRegisterActiveActionEvent(ActiveAction activeAction)
    {
        if (activeAction != null)
        {
            m_activeAction -= activeAction;
        }
    }

    //Registramos los eventos     
    public void RegisterDesactiveActionEvent(DesactiveAction desactiveAction)
    {
        if (desactiveAction != null)
        {
            m_desactiveAction += desactiveAction;
        }
    }
    //Desregistro de eventos.
    public void UnRegisterDesactiveActionEvent(DesactiveAction desactiveAction)
    {
        if (desactiveAction != null)
        {
            m_desactiveAction -= desactiveAction;
        }
    }
    #endregion


    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void SetInput(Cable cableToSet)
    {
        cable = cableToSet;
    }

    public void ActivateParentNode()
    {
        if (m_activeAction!=null) m_activeAction();
    }

    public void DesactivateParentNode()
    {
        ReleaseCable();
        if (m_desactiveAction != null) m_desactiveAction();
    }

    public void CableConnected()
    {
        audioSource.PlayOneShot(plug);
        GetComponent<SpriteRenderer>().sprite = cableConnected;
    }

    public void ReleaseCable()
    {
        audioSource.PlayOneShot(unplug);
        GetComponent<SpriteRenderer>().sprite = cableOut;
    }

    public Cable GetCable()
    {
        return cable;
    }
}



