﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cable : MonoBehaviour
{
    CableVisual cableVisual;
    public OutputCable origin;
    public InputCable ending;
    private float distance;

    void Start()
    {
        cableVisual = GetComponentInChildren<CableVisual>();
    }
    private void Update()
    {
    }

    public void CreateCable(OutputCable nodeOrigin, float distance)
    {
        origin = nodeOrigin;
        this.distance = distance;
    }
    
    public void GrabCable()
    {
        cableVisual.GrabCable();
    }

    public void ConnectCable(InputCable nodeEnd)
    {
        if (ending != null) return;
        ending = nodeEnd;

        origin.SetOutput(this); 
        ending.SetInput(this);

        origin.CableConnected();
        ending.CableConnected();

        if (ending != null)
            ending.ActivateParentNode();

        cableVisual.ConnectCable(ending.transform.position);
    }

    public void DisconnectCable()
    {
        cableVisual.GrabCable();
        if(ending!=null)
            ending.DesactivateParentNode();
        ending = null;

    }

    public void MoveCable(Vector2 pos)
    {
        if (Vector2.Distance(origin.transform.position, pos) > distance) GameObject.FindGameObjectWithTag("Player").GetComponent<ChispasController>().ReleaseCable();
        cableVisual.MoveCable(pos);
    }

    public void DropCable()
    {
        cableVisual.DropCable();
        origin.GetComponent<OutputCable>().DropCable();
    }

}
