﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CounterTime : MonoBehaviour {

    public float timer;
    public Text timeText;

	// Use this for initialization
	void Start () {
        timer = 122.0f;
    }
	
	// Update is called once per frame
	void Update () {
        timer -= Time.deltaTime;
        if(timer>=0)
            TimeToText(timer);
    }

    private void TimeToText(float time)
    {

        int min = (int) (time / 60);
        int seg = (int)(time % 60);
        string segText = seg.ToString();

        if (seg < 10)
            segText = "0" + seg;

        timeText.text = min + ":" + segText; 

    }
}
