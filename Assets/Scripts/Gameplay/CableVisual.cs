﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CableVisual : MonoBehaviour
{
    [SerializeField]
    Transform chispas;
    [SerializeField]
    Transform endLink;
    [SerializeField]
    List<Transform> links;
    LineRenderer lineRenderer;

    float globalTimer;

    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        chispas = GameObject.FindGameObjectWithTag("Player").transform;
        lineRenderer.enabled = false;
        for (int i = 1; i < links.Count; i++)
        {
            links[i].GetComponent<HingeJoint2D>().connectedAnchor = new Vector2(0, 0);
        }
    }
    
    public void GrabCable()
    {
        lineRenderer.enabled = true;
        for (int i = 1; i < links.Count; i++)
        {
            links[i].GetComponent<HingeJoint2D>().connectedAnchor = new Vector2(0, 0.75f);
        }
        endLink.transform.position = chispas.position;
    }

    public void DropCable()
    {
        Debug.Log("SOLTANDO CABLE");
        //for (int i = 1; i < links.Count; i++)
        //{
        //    links[i].GetComponent<HingeJoint2D>().connectedAnchor = new Vector2(0, 0);
        //}
        StartCoroutine(WaitToDisableCable());
    }
    
    IEnumerator WaitToDisableCable()
    {
        globalTimer = 0;
        List<Transform> linksOrigin = links;
        while(globalTimer <= 0.3f)
        {
            globalTimer += Time.deltaTime;
            for (int i = 0; i < links.Count; i++)
            {
                if (i > 0)
                    links[i].position = Vector2.Lerp(linksOrigin[i].position, links[0].position, globalTimer / 0.3f);
                lineRenderer.SetPosition(i, links[i].position);
            }
            yield return null;
        }
        lineRenderer.enabled = false;
    }

    public void MoveCable(Vector2 pos)
    {
        endLink.position = pos;
        for (int i = 0; i<links.Count; i++)
        {
            lineRenderer.SetPosition(i, links[i].position);
        }
    }

    public void ConnectCable(Vector2 pos)
    {
        StartCoroutine(ConnectCableCoroutine(pos));
    }

    IEnumerator ConnectCableCoroutine(Vector2 pos)
    {
        globalTimer = 0;
        Vector2 startPos = endLink.position;
        while (globalTimer <= 0.5f)
        {
            globalTimer += Time.deltaTime;
            Debug.Log("CONECTANDO CABLE");
            MoveCable(Vector2.Lerp(startPos, pos, 1/0.5f));
            yield return null;
        }
    }
}
