﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void ActiveAction();
public delegate void DesactiveAction();

public abstract class EnergyNode : MonoBehaviour
{
    protected SpriteRenderer nodeSprite;
    public InputCable inputCable;

    void Start()
    {
        nodeSprite = GetComponent<SpriteRenderer>();
        if (inputCable)
        {
            //Registramos los eventos de accion
            inputCable.RegisterActiveActionEvent(OnActionActive);
            inputCable.RegisterDesactiveActionEvent(OnActionDesactive);
        }
    }

    private void OnDestroy()
    {
        if (inputCable)
        {
            //Desregistramos los eventos de accion
            inputCable.UnRegisterActiveActionEvent(OnActionActive);
            inputCable.UnRegisterDesactiveActionEvent(OnActionDesactive);
        }
    }

    public abstract void OnActionActive();

    public abstract void OnActionDesactive();

}

