﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : EnergyNode {

    public override void OnActionActive()
    {
        gameObject.SetActive(false);
    }

    public override void OnActionDesactive()
    {
        gameObject.SetActive(true);
    }
}
