﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteLayering : MonoBehaviour
{
    [SerializeField]
    bool staticObject = true;

    SpriteRenderer spriteRenderer;

    // Use this for initialization
    void Start ()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sortingOrder = -(int)(transform.position.y * 5);	
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(!staticObject)
        {
            spriteRenderer.sortingOrder = -(int)(transform.position.y * 5);
        }
	}
}
