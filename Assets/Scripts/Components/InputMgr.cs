﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputMgr : MonoBehaviour {

    public Dictionary<string, InputPlayerController> controllers;

    public InputMgr()
    {
        controllers = new Dictionary<string, InputPlayerController>();
    }

    public InputPlayerController GetInputPlayerController(string player)
    {
        InputPlayerController result;
        controllers.TryGetValue(player,out result);
        return result;
    }

    public void AddInputPlayerController(string player, InputPlayerController inputPlayerController)
    {
        controllers.Add(player, inputPlayerController);
    }

    public void RemoveInputPlayerController(string player)
    {
        controllers.Remove(player);
    }


}
