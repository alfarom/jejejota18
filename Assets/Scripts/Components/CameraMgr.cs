﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMgr : MonoBehaviour {

    public Transform newPosition;
    public GameObject activeObject, desactiveObject;

    public bool player1, player2;

    public GameObject[] laserObjects;

    void Start()
    {
        laserObjects = GameObject.FindGameObjectsWithTag("Laser");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            player1 = true;
        }
              
        if(collision.CompareTag("Player2"))
        {
            player2 = true;
        }

        if (player1 && player2)
        {
            activeObject.SetActive(true);
            if(newPosition!=null)
                StartCoroutine(Move(newPosition.position));
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            player1 = false;
        }

        if (collision.CompareTag("Player2"))
        {
            player2 = false;

        }
    }

    IEnumerator Move(Vector3 pos)
    {
        float timer = 0.0f;
        Vector3 initialPos = Camera.main.transform.position;
        //Parar los lasers de
        foreach(GameObject laser in laserObjects)
        {
            laser.GetComponent<AudioSource>().Stop();
        }
        while (timer < 1.0f)
        {
            timer += Time.deltaTime;
            Camera.main.transform.position = Vector3.Lerp(initialPos, pos, timer);
            yield return null;
        }
        if (desactiveObject != null) desactiveObject.SetActive(false);
        gameObject.SetActive(false);

    }
}
